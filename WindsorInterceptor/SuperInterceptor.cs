using System;
using Castle.DynamicProxy;

namespace WindsorInterceptor
{
    public class SuperInterceptor : ISuperInterceptor
    {
        public void Intercept(IInvocation invocation)
        {
            Console.Out.WriteLine("Interceptor before");

            invocation.Proceed();
            
            Console.Out.WriteLine("Interceptor after");
            var currentProfit = (long)invocation.ReturnValue;
            invocation.ReturnValue = currentProfit * 2;
        }
    }

    public interface ISuperInterceptor : IInterceptor
    {
    }
}