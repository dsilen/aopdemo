using System.Reflection;
using Castle.Core;
using Castle.DynamicProxy;
using Castle.MicroKernel.Registration;
using Castle.Windsor;

namespace WindsorInterceptor
{
    public static class ContainerInitializer
    {

        private static readonly Assembly ThisAssembly = Assembly.GetAssembly(typeof(ContainerInitializer));

        public static IWindsorContainer Init()
        {
            var container = new WindsorContainer();

            container.RegisterInterceptor();

            var autoClasses = AutoClasses();
            container.Register(
                autoClasses
                    .Configure(c => 
                    {
                        var _ = c.Interceptors(InterceptorReference.ForType<ISuperInterceptor>()).Anywhere; 
                    }));
            
            return container;
        }

        private static BasedOnDescriptor AutoClasses()
        {
            return Classes.FromAssembly(ThisAssembly).BasedOn<IAutoconfigComponent>().WithServiceAllInterfaces();
        }

        private static void RegisterInterceptor(this WindsorContainer container)
        {
            container.Register(
                Component.For<ISuperInterceptor>()
                    .ImplementedBy<SuperInterceptor>());
        }
    }
    
    public interface IAutoconfigComponent {}
}