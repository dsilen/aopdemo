﻿using System;
using Castle.Windsor;

namespace WindsorInterceptor
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Out.WriteLine($"Starting");
            using var container = ContainerInitializer.Init();

            var businessThing = container.Resolve<IInjectedBusinessThing>();

            var katching = businessThing.MakeProfit();
            Console.Out.WriteLine($"The good stuff: {katching}");
        }
    }
}
    