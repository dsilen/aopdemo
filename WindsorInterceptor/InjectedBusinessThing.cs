using System;

namespace WindsorInterceptor
{
    public class InjectedBusinessThing : IInjectedBusinessThing, IAutoconfigComponent
    {
        public long MakeProfit()
        {
            Console.Out.WriteLine("Making profit");
            return 10;
        }
    }

    public interface IInjectedBusinessThing
    {
        long MakeProfit();
    }
}