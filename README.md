Aspect Oriented Programming
=====

Intro
---
AOP handlar om att man dynamiskt injicerar kod i sin programkod.
Det kan tex vara att man före varje metod-anrop (i en klass, namespace, nuget, hel lösning etc etc)
först hamnar i en "gemensam" metod som kollar behörigheter.
Meta programmering. Lite som macros i LISP, där dessa ju går in och ändrar i programkoden.

Det finns lite olika sätt att lösa detta. Tex att man flaggar med attribut egenskaper
man vill ska läggas till med "aspects", eller att man ärver av nån klass,
implementerar nåt interface osv.
Eller helt enkelt att alla klasser och metoder checkas överallt. Bra för debug-loggning tex.

Den kod som injiceras kallas för Aspects.

Titta på exempel så blir det klarare.

Windsor-koden anropar en metod och gör nån form av post-processning.

Postsharop-koden loggar före och efter att en metod anropas.

Vanliga saker man kan/ vill göra:
 * minimera boilerplate-kod.
   Exempel att man skriver en automatisk "ToString" på dataklasser som tittar vad det
   finns för publika properties i klassen och skriver en tostring med dessa.
 * se till att krav på pre eller post-processning som finns alltid körs.
   tex validera user-requests på publikt nybara api:er. Att kod körs innan man
   kommer in i själva handlern som först gör validering och kastar exception om det 
   inte är ok.
 * (Fylla i dependencies) Gör man bättre med dependency injection.

Länkar
---

* Source code generators. Ny feature som finns i C# 9.
  Skapar nya programkod dynamiskt. Inte superbra till AOP
  eftersom det inte går att ersätta kod. Bara skapa nytt.
  Man kan därför tex inte enkelt ändra beteende på property-setters.
  Men annars rätt coolt. Kan ändras för det är f.f WIP.
   
   https://devblogs.microsoft.com/dotnet/introducing-c-source-generators/


* AOP med Castle Windsor interceptors
   Windsor är ett dependency injection ramverk som även går använda till
   AOP med lite tweakning. Man kan registrera s.k interceptors som
  anropas istället för den kod som resolvas.

  Inne i interceptorn kan man sedan välja att helt byta ut implementationen,
  göra något innan och sedan anropa den "riktiga metoden" eller göra något
   efter anropet, tex ta hand om felhantering.
 
   https://github.com/castleproject/Windsor/blob/master/docs/orphan-introduction-to-aop-with-castle.md

  * Postsharp
     Postsharp är en renodlad AOP lösning som funnits ca 15 år.
    Där bygger de på ett "post kompileringssteg" som letar efter aspects
     och bygger om kod så de anropas som tänkt.
     Snyggast lösning, men det kan vara strul med deras licenser.
     Det finns gratis-versioner som gör det man behöver, men är begränsat
    antingen till hur mycket kod som får finnas i lösningen eller
    vilka metoder som får användas.
   
   Finns många färdiga aspects för vanliga problem, tex implentera INotifyPropertyChanged

    https://www.postsharp.net/
   

* Diverse
  - https://medium.com/swlh/thinking-beyond-roslyn-source-generators-and-aspect-oriented-programming-3e42d58c37ac
  - https://levelup.gitconnected.com/aop-dotnet-applications-67c6d94c08b0
