﻿using System;

namespace PostsharpDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            var foo = new BusinessLogicThingy();
            Console.WriteLine("Start program");
            foo.BusinessMethod("businessy stuff");
            Console.WriteLine("End program");
        }
    }
}