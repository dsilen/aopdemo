using System;

namespace PostsharpDemo
{
    [EntryExitLoggingAspect]
    public class BusinessLogicThingy
    {
        public void BusinessMethod(string parameter)
        {
            Console.WriteLine($"Inside BusinessMethod, doing things with {parameter}");
        }
    }
}