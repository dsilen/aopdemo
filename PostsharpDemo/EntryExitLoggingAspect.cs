using System;
using System.Linq;
using PostSharp.Aspects;
using PostSharp.Serialization;

namespace PostsharpDemo
{
    [PSerializable]
    public class EntryExitLoggingAspect : OnMethodBoundaryAspect
    {
        public override void OnEntry(MethodExecutionArgs args)
        {
            Console.WriteLine($"The {args.Method.Name} method being entered. Called with args {string.Join(", ",args.Arguments.Select(x => $"\"{x}\""))}");
        }
        
        public override void OnExit(MethodExecutionArgs args)
        {
            Console.WriteLine($"The {args.Method.Name} method has been exited.");
        }
    }
}